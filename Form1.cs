﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TicToc
{
    public partial class Form1 : Form
    {
        public const int ChessFields = 8;

        public ChessBoard Board;
        public GlobalConfig Config = new GlobalConfig();

        public readonly DrawnButton<Form1> ButtonUndo;

        public readonly DrawnButton<Form1> ButtonTie;
        // public readonly DrawnButton<Form1> ButtonSave;
        // public readonly DrawnButton<Form1> ButtonLoad;

        public Form1()
        {
            ButtonUndo = new DrawnButton<Form1>(new Rectangle(10, 10, 50, 20), "Undo", this, UndoClick);
            ButtonTie = new DrawnButton<Form1>(new Rectangle(10, 60, 50, 20), "Tie!", this, CallTie);
            //ButtonSave = new DrawnButton<Form1>(new Rectangle(10, 60, 50, 20), "Save", this, SaveToFile);
            //ButtonLoad = new DrawnButton<Form1>(new Rectangle(10, 110, 50, 20), "Load", this, ReadFromFile);

            InitializeComponent();

            if (!(File.Exists(GetGlobalSaveFile()) && ReadFromFile(this)))
            {
                SetupNewGame();
            }

            OnResize();

            panel1.MouseClick += OnClick;
            KeyDown += OnKeyDown;

            panel1.Invalidate();
        }

        public void SetupNewGame()
        {
            var frm2 = new FormPreGameSetup(this, Config);
            DialogResult dr = frm2.ShowDialog(this);
            if (dr == DialogResult.Cancel)
            {
                Close();
                return;
            }

            if (dr == DialogResult.Yes)
            {
                Board = new ChessBoard(ChessFields, false, Config.FieldColour == FieldColour.White, this);
                Board.ResetGame(Config.PlayerTop == Config.PlayerWhite);
                Config.GameInProgress = true;
                Bounds = Bounds;
            }
        }

        public void GameEnded(bool won, bool whiteWon)
        {
            Config.GameInProgress = false;
            if (won)
            {
                if (whiteWon == (Config.PlayerWhite == Player.One))
                {
                    Config.Player1Wins++;
                }
                else
                {
                    Config.Player2Wins++;
                }
            }

            SetupNewGame();
        }

        private static void UndoClick(DrawnButton<Form1> button)
        {
            button.Arg.Board.UndoMove();

            button.Arg.panel1.Invalidate();
        }

        private static void CallTie(DrawnButton<Form1> button)
        {
            button.Arg.GameEnded(false, false);
        }

        private void OnClick(Object sender, MouseEventArgs e)
        {
            if (new Rectangle(Board.Origin, new Size(Board.Length, Board.Length)).Contains(e.Location))
            {
                //               Console.WriteLine("inside board: {0} {1}", e.X, e.Y);
                var relative = e.Location - new Size(Board.Origin);
                Board.HandleClick(relative.X, relative.Y,
                    e.Button == MouseButtons.Left && e.Button != MouseButtons.Right);
            }

            ButtonUndo.TryClick(e.Location);
            ButtonTie.TryClick(e.Location);
            //ButtonLoad.TryClick(e.Location);
            //ButtonSave.TryClick(e.Location);
        }

        private void OnKeyDown(Object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D:
                    Board.RecalculateMoves();
                    break;
                case Keys.P:
                    Console.WriteLine("pressed p");
                    break;
                case Keys.W:
                    GameEnded(true, Board.WhitePlaying);
                    break;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            SaveToFile(this);
            base.OnClosed(e);
        }

        string GetSaveFile()
        {
            return "savefile.txt";
        }

        string GetGlobalSaveFile()
        {
            return "config.txt";
        }

        static void SaveToFile(Form1 form)
        {
            var global = form.Config.Serialise();
            File.WriteAllText(form.GetGlobalSaveFile(), global);

            if (form.Config.GameInProgress)
            {
                var serialised = form.Board.Serialise();
                File.WriteAllText(form.GetSaveFile(), serialised);
            }
        }

        static bool ReadFromFile(Form1 form)
        {
            var global = File.ReadAllText(form.GetGlobalSaveFile());
            GlobalConfig.Deserialise(form.Config, new StringReader(global), form);

            if (form.Config.GameInProgress)
            {
                //TODO read global config
                var serialised = File.ReadAllText(form.GetSaveFile());
                form.Board = ChessBoard.Deserialise(new StringReader(serialised), form);
                return true;
            }

            return false;
        }

        private void OnResize()
        {
            int w = Width, h = Height;
            panel1.SetBounds(0, 0, w, h);

            int midh = h / 2, midw = w / 2;

            int a = Math.Min(w, h) * 3 / 4;
            int mida = a / 2;

            Board.Origin = new Point(midw - mida, midh - mida);
            Board.Length = a;
            Board.Segment = a / (double) Board.Size;
        }

        [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            Board.Paint(g);
            ButtonUndo.Paint(g);
            ButtonTie.Paint(g);
            //ButtonLoad.Paint(g);
            //ButtonSave.Paint(g);
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            OnResize();
            panel1.Invalidate();
        }
    }

    public class GlobalConfig
    {
        public String Player1Name, Player2Name;
        public Player PlayerWhite, PlayerTop;
        public FieldColour FieldColour;

        public bool GameInProgress;
        public int Player1Wins, Player2Wins;

        public String Serialise()
        {
            var w = new StringBuilder();
            w.AppendLine(Player1Name);
            w.AppendLine(Player2Name);
            w.AppendLine(((int) PlayerWhite).ToString());
            w.AppendLine(((int) PlayerTop).ToString());
            w.AppendLine(((int) FieldColour).ToString());
            w.AppendLine(GameInProgress.ToString());
            w.AppendLine(Player1Wins.ToString());
            w.AppendLine(Player2Wins.ToString());
            return w.ToString();
        }

        public static GlobalConfig Deserialise(GlobalConfig conf, StringReader r, Form1 form)
        {
            conf.Player1Name = r.ReadLine();
            conf.Player2Name = r.ReadLine();
            conf.PlayerWhite = (Player) int.Parse(r.ReadLine());
            conf.PlayerTop = (Player) int.Parse(r.ReadLine());
            conf.FieldColour = (FieldColour) int.Parse(r.ReadLine());
            conf.GameInProgress = bool.Parse(r.ReadLine());
            conf.Player1Wins = int.Parse(r.ReadLine());
            conf.Player2Wins = int.Parse(r.ReadLine());

            return conf;
        }
    }

    public enum FieldColour
    {
        None = 0,
        White,
        Black
    }

    public enum Player
    {
        None = 0,
        One,
        Two
    }

    public class ChessMove
    {
        private ChessField _origin;
        public bool PlayedByWhite;
        public bool Promoted;

        public readonly ChessPiece Piece;
        public readonly ChessField Target;
        public readonly ChessPiece Capture;

        public ChessMove(ChessPiece piece, ChessField target, ChessPiece capture = null)
        {
            Piece = piece;
            Target = target;
            Capture = capture;
        }

        public void Execute()
        {
            _origin = Piece.Field;
            PlayedByWhite = Piece.Field.Board.WhitePlaying;

            Piece.Move(Target);
            Capture?.Capture();

            if (Target.OffsetY == (Target.Board.WhiteLow ^ Piece.White ? 0 : Target.Board.Size - 1))
            {
                Piece.Queen = true;
                Promoted = true;
            }

            Target.Board.MoveHistory.Push(this);

            Target.Board.RecalculateMoves();
        }

        public void Undo()
        {
            _origin.Board.WhitePlaying = PlayedByWhite;

            Piece.Move(_origin);
            Capture?.UndoCapture();

            if (Promoted) Piece.Queen = false;

            _origin.Board.Selected = Piece;
            _origin.Board.RecalculateMoves();
        }
    }

    public class ChessPiece
    {
        public ChessField Field;
        public bool White;
        public bool Queen;

        public List<ChessMove> CachedAvailableMoves;

        public char Serialise()
        {
            if (White)
                return Queen ? 'W' : 'w';

            return Queen ? 'B' : 'b';
        }

        public static ChessPiece Parse(char c, ChessField field)
        {
            switch (c)
            {
                case 'w':
                    return new ChessPiece(true, field);
                case 'W':
                    return new ChessPiece(true, field, true);
                case 'b':
                    return new ChessPiece(false, field);
                case 'B':
                    return new ChessPiece(false, field, true);
                default:
                    return null;
            }
        }

        public ChessPiece(bool white, ChessField field, bool queen = false)
        {
            White = white;
            Field = field;
            Queen = queen;
        }

        public void Draw(Graphics g, Rectangle fieldBb)
        {
            if (White == Field.White)
                g.DrawEllipse(White ? Pens.Black : Pens.White, fieldBb);
            else
                g.FillEllipse(White ? Brushes.White : Brushes.Black, fieldBb);

            if (Queen)
            {
                fieldBb.Inflate(-fieldBb.Width / 3, -fieldBb.Height / 3);
                g.FillEllipse(White ? Brushes.Black : Brushes.White, fieldBb);
            }
        }

        public void Move(ChessField target)
        {
            target.Piece = this;
            Field.Piece = null;
            Field = target;
        }

        public void Capture()
        {
            Field.Board.Captures.Add(this);
            Field.Piece = null;
        }

        public void UndoCapture()
        {
            Field.Board.Captures.Remove(this);
            Field.Piece = this;
        }

        public List<ChessMove> AvailableMoves()
        {
            var list = new List<ChessMove>();

            var ymotion = Field.Board.WhitePlaying == Field.Board.WhiteLow ? 1 : -1;

            var xm = TraceMove(-1, ymotion);
            var xp = TraceMove(1, ymotion);

            if (xm != null) list.AddRange(xm);
            if (xp != null) list.AddRange(xp);

            if (Queen)
            {
                var mxm = TraceMove(-1, -ymotion);
                var mxp = TraceMove(1, -ymotion);

                if (mxm != null) list.AddRange(mxm);
                if (mxp != null) list.AddRange(mxp);
            }

            if (list.Exists(m => m.Capture != null))
                list.RemoveAll(m => m.Capture == null);

            return list;
        }

        List<ChessMove> TraceMove(int xmotion, int ymotion)
        {
            ChessPiece capture = null;

            ChessField target = Field;

            List<ChessField> queenTargets = new List<ChessField>();

            queenCycle:
            target = target.WithOffset(xmotion, ymotion);
            if (target == null)
                goto end;

            if (target.Piece != null)
            {
                //potential capture
                capture = target.Piece;
                target = target.WithOffset(xmotion, ymotion);
                if (target == null)
                    goto end;
                if (target.Piece != null || capture.White == Field.Board.WhitePlaying)
                {
                    //cannot capture
                    goto end;
                }

                //capture
            }
            else if (Queen)
            {
                queenTargets.Add(target);
                goto queenCycle;
            }

            // regular move, or potentially capture
            return new List<ChessMove> {new ChessMove(this, target, capture)};

            end:
            return Queen ? queenTargets.Select(f => new ChessMove(this, f)).ToList() : null;
        }
    }

    public class ChessField
    {
        public readonly ChessBoard Board;
        public readonly bool White;
        public ChessPiece Piece;
        public readonly int OffsetX;
        public readonly int OffsetY;

        public ChessField(int offsetX, int offsetY, bool white, ChessBoard board)
        {
            OffsetY = offsetY;
            OffsetX = offsetX;
            White = white;
            Board = board;
            Piece = null;
        }

        public Point AbsPos()
        {
            int offX = (int) (Board.Segment * OffsetX);
            int offY = (int) (Board.Segment * OffsetY);
            var p = Board.Origin;
            p.X += offX;
            p.Y += offY;
            return p;
        }

        public void Paint(Graphics g)
        {
            var pos = AbsPos();
            var size = (int) Board.Segment;
            var rect = new Rectangle(pos, new Size(size, size));
            g.FillRectangle(White ? Brushes.White : Brushes.Black, rect);

            rect.Inflate(-rect.Width / 10, -rect.Height / 10);
            Piece?.Draw(g, rect);
        }

        public ChessField WithOffset(int x, int y)
        {
            x += OffsetX;
            y += OffsetY;

            if (x < 0 || y < 0 || x >= Board.Size || y >= Board.Size)
            {
                return null;
            }

            return Board.Fields[x, y];
        }
    }

    public class ChessBoard
    {
        public readonly Form1 Form;

        public Point Origin;
        public int Length;
        public double Segment;
        public readonly int Size;
        public readonly ChessField[,] Fields;

        public bool WhiteLow;
        public bool UseWhite;
        public bool OriginWhite;

        public ChessPiece Selected;

        public bool WhitePlaying = true;

        public List<ChessPiece> AllowedSelection;

        public List<ChessPiece> Captures = new List<ChessPiece>();

        public readonly Stack<ChessMove> MoveHistory = new Stack<ChessMove>();

        public String Serialise()
        {
            var w = new StringBuilder();
            w.AppendLine(Size.ToString());
            w.AppendLine(OriginWhite.ToString());
            w.AppendLine(WhiteLow.ToString());
            w.AppendLine(UseWhite.ToString());
            w.AppendLine(WhitePlaying.ToString());
            for (var y = 0; y < Size; y++)
            {
                for (var x = 0; x < Size; x++)
                {
                    w.Append(Fields[x, y].Piece?.Serialise() ?? ' ');
                }

                w.AppendLine();
            }

            return w.ToString();
        }

        public static ChessBoard Deserialise(StringReader r, Form1 form)
        {
            int size = int.Parse(r.ReadLine());
            var originWhite = bool.Parse(r.ReadLine());
            var whiteLow = bool.Parse(r.ReadLine());
            var useWhite = bool.Parse(r.ReadLine());
            var whitePlaying = bool.Parse(r.ReadLine());
            var c = new ChessBoard(size, originWhite, useWhite, form)
            {
                WhiteLow = whiteLow,
                WhitePlaying = whitePlaying
            };
            for (var y = 0; y < size; y++)
            {
                var line = r.ReadLine().ToCharArray();
                for (int x = 0; x < size; x++)
                {
                    c.Fields[x, y].Piece = ChessPiece.Parse(line[x], c.Fields[x, y]);
                }
            }

            c.RecalculateMoves();

            return c;
        }


        public ChessBoard(int size, bool originWhite, bool useWhite, Form1 form)
        {
            Size = size;
            Fields = new ChessField[size, size];

            OriginWhite = originWhite;
            UseWhite = useWhite;
            Form = form;

            {
                var genAlt = GenerateAlternating(originWhite);
                for (int x = 0; x < size; x++)
                {
                    for (int y = 0; y < size; y++)
                    {
                        genAlt.MoveNext();
                        Fields[x, y] = new ChessField(x, y, genAlt.Current, this);
                    }

                    genAlt.MoveNext();
                }

                genAlt.Dispose();
            }
        }

        public bool UndoMove()
        {
            if (MoveHistory.Count > 0)
            {
                MoveHistory.Pop()?.Undo();
                return true;
            }

            return false;
        }

        public void RecalculateMoves()
        {
            AllowedSelection = null;
            bool hasMoves = false;
            foreach (var field in Fields)
            {
                if (field.Piece?.White == WhitePlaying)
                {
                    var moves = field.Piece.CachedAvailableMoves = field.Piece.AvailableMoves();

                    if (moves.Count != 0)
                    {
                        hasMoves = true;
                    }

                    if (moves.Exists(m => m.Capture != null))
                    {
                        if (AllowedSelection == null)
                            AllowedSelection = new List<ChessPiece>();
                        AllowedSelection.Add(field.Piece);
                    }
                }
            }

            if (!hasMoves)
            {
                Form.GameEnded(true, !WhitePlaying);
            }
        }

        public void ResetGame(bool whiteLow)
        {
            WhiteLow = whiteLow;
            WhitePlaying = true;

            foreach (var field in Fields)
            {
                field.Piece = null;
                if (field.White == UseWhite)
                {
                    if (field.OffsetY == 0 || field.OffsetY == 1)
                        field.Piece = new ChessPiece(whiteLow, field);
                    else if (field.OffsetY == Size - 1 || field.OffsetY == Size - 2)
                        field.Piece = new ChessPiece(!whiteLow, field);
                }
            }

            RecalculateMoves();
        }

        public void Paint(Graphics g)
        {
            foreach (var field in Fields)
            {
                field.Paint(g);
            }

            if (Selected != null)
            {
                foreach (var field in Selected.CachedAvailableMoves.Select(m => m.Target).Append(Selected.Field))
                {
                    SolidBrush selection = new SolidBrush(Color.FromArgb(128, Color.RoyalBlue));
                    var len = (int) Segment;
                    var rect = new Rectangle(field.AbsPos(), new Size(len, len));
                    g.FillRectangle(selection, rect);
                }
            }
        }

        public static int Clamp(int v, int min, int max)
        {
            return v > max ? max : v < min ? min : v;
        }

        public void HandleClick(int x, int y, bool left)
        {
            var seg = (int) Segment;
            var offsetX = Clamp(x / seg, 0, Size - 1);
            var offsetY = Clamp(y / seg, 0, Size - 1);

            //            Console.WriteLine("board click: {0} {1} _ {4} _ {2} {3}", x, y, offsetX, offsetY, length);

            var field = Fields[offsetX, offsetY];

            if (!left)
            {
                if (field.Piece == null)
                {
                    field.Piece = new ChessPiece(true, field);
                }
                else if (field.Piece.White)
                {
                    if (!field.Piece.Queen)
                    {
                        field.Piece.Queen = true;
                    }
                    else
                    {
                        field.Piece.Queen = false;
                        field.Piece.White = false;
                    }
                }
                else
                {
                    if (!field.Piece.Queen)
                    {
                        field.Piece.Queen = true;
                    }
                    else
                    {
                        field.Piece = null;
                    }
                }

                RecalculateMoves();

                Form.panel1.Invalidate();
                return;
            }

            if (field.Piece != null)
            {
                if (field.Piece.White == WhitePlaying)
                {
                    if (AllowedSelection?.Contains(field.Piece) ?? true)
                    {
                        Selected = field.Piece;
                        Form.panel1.Invalidate();
                    }
                }
            }
            else if (Selected != null)
            {
                var move = Selected.CachedAvailableMoves.FirstOrDefault(m => m.Target == field);
                if (move != null)
                {
                    move.Execute();
                    Selected = null;
                    if (move.Capture != null)
                    {
                        var moves = move.Piece.CachedAvailableMoves;
                        if (moves.Exists(m => m.Capture != null))
                        {
                            Selected = move.Piece;
                            AllowedSelection = new List<ChessPiece> {Selected};
                        }
                        else
                        {
                            WhitePlaying = !WhitePlaying;
                        }
                    }
                    else
                    {
                        WhitePlaying = !WhitePlaying;
                    }

                    RecalculateMoves();

                    Form.panel1.Invalidate();
                }
            }
        }

        public static IEnumerator<bool> GenerateAlternating(bool start)
        {
            while (true)
            {
                yield return start = !start;
            }

            // ReSharper disable once IteratorNeverReturns
        }
    }

    public class DrawnButton<T>
    {
        public readonly Rectangle Bounds;

        public Brush Background;
        public Pen Border;

        public String Text;
        public Font Font;
        public Brush TextBrush;

        public Action<DrawnButton<T>> OnClick;

        public readonly T Arg;

        public DrawnButton(Rectangle bounds, string text, T arg, Action<DrawnButton<T>> onClick,
            Brush background = null,
            Pen border = null, Font font = null,
            Brush textBrush = null)
        {
            Arg = arg;
            Bounds = bounds;
            Background = background ?? Brushes.White;
            Border = border ?? Pens.Black;
            Text = text;
            OnClick = onClick;
            Font = font ?? new Font("sansSerif", 12);
            TextBrush = textBrush ?? Brushes.Black;
        }

        public void Paint(Graphics g)
        {
            g.FillRectangle(Background, Bounds);
            g.DrawRectangle(Border, Bounds);
            g.DrawString(Text, Font, TextBrush, Bounds);
        }

        public bool TryClick(Point p)
        {
            if (!Bounds.Contains(p)) return false;
            OnClick?.Invoke(this);
            return true;
        }
    }
}