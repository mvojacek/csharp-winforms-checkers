﻿using System;
using System.Windows.Forms;

namespace TicToc
{
    public partial class FormPreGameSetup : Form
    {
        public Form1 Form1;
        public GlobalConfig Config;

        public FormPreGameSetup(Form1 form1, GlobalConfig config)
        {
            Form1 = form1;
            Config = config;

            InitializeComponent();

            if (config.Player1Name != null)
            {
                scoreTextLabel1.Text = player1NameBox.Text = config.Player1Name;
            }
            else
            {
                config.Player1Name = scoreTextLabel1.Text = player1NameBox.Text;
            }

            if (config.Player2Name != null)
            {
                scoreTextLabel2.Text = player2NameBox.Text = config.Player2Name;
            }
            else
            {
                config.Player2Name = scoreTextLabel2.Text = player2NameBox.Text;
            }

            if (config.PlayerTop != Player.None)
            {
                topsidePlayerCombo.SelectedIndex = -1 + (int) config.PlayerTop;
            }

            if (config.PlayerWhite != Player.None)
            {
                whitePlayerCombo.SelectedIndex = -1 + (int) config.PlayerWhite;
            }

            if (config.FieldColour != FieldColour.None)
            {
                playedFieldColourCombo.SelectedIndex = -1 + (int) config.FieldColour;
            }

            scoreLabel1.Text = config.Player1Wins.ToString();
            scoreLabel2.Text = config.Player2Wins.ToString();
        }

        private void Player1NameBox_TextChanged(object sender, EventArgs e)
        {
            Config.Player1Name = scoreTextLabel1.Text = player1NameBox.Text;
        }

        private void Player2NameBox_TextChanged(object sender, EventArgs e)
        {
            Config.Player2Name = scoreTextLabel2.Text = player2NameBox.Text;
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Config.Player1Name))
            {
                ShowError("Missing player 1 name");
                return;
            }

            if (string.IsNullOrEmpty(Config.Player2Name))
            {
                ShowError("Missing player 2 name");
                return;
            }

            if (Config.FieldColour == FieldColour.None)
            {
                ShowError("Missing field colour");
                return;
            }

            if (Config.PlayerTop == Player.None)
            {
                ShowError("Missing top player");
                return;
            }

            if (Config.PlayerWhite == Player.None)
            {
                ShowError("Missing white player");
                return;
            }

            DialogResult = DialogResult.Yes;
            Close();
        }

        private void ShowError(string error)
        {
            MessageBox.Show(error);
        }

        private void PlayedFieldColourCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Config.FieldColour = (FieldColour) playedFieldColourCombo.SelectedIndex + 1;
        }

        private void TopsidePlayerCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Config.PlayerTop = (Player) topsidePlayerCombo.SelectedIndex + 1;
        }

        private void WhitePlayerCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Config.PlayerWhite = (Player) whitePlayerCombo.SelectedIndex + 1;
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            Config.Player2Wins = Config.Player1Wins = 0;
            scoreLabel1.Text = scoreLabel2.Text = "0";
        }
    }
}