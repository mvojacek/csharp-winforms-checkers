﻿using System.ComponentModel;
using System.Windows.Forms;

namespace TicToc
{
    partial class FormPreGameSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.player1NameBox = new System.Windows.Forms.TextBox();
            this.player2NameBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.whitePlayerCombo = new System.Windows.Forms.ComboBox();
            this.topsidePlayerCombo = new System.Windows.Forms.ComboBox();
            this.playedFieldColourCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.scoreLabel1 = new System.Windows.Forms.Label();
            this.scoreTextLabel1 = new System.Windows.Forms.Label();
            this.scoreTextLabel2 = new System.Windows.Forms.Label();
            this.scoreLabel2 = new System.Windows.Forms.Label();
            this.resetButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player 1 name:";
            // 
            // player1NameBox
            // 
            this.player1NameBox.Location = new System.Drawing.Point(135, 38);
            this.player1NameBox.Name = "player1NameBox";
            this.player1NameBox.Size = new System.Drawing.Size(100, 20);
            this.player1NameBox.TabIndex = 1;
            this.player1NameBox.Text = "Player1";
            this.player1NameBox.TextChanged += new System.EventHandler(this.Player1NameBox_TextChanged);
            // 
            // player2NameBox
            // 
            this.player2NameBox.Location = new System.Drawing.Point(135, 64);
            this.player2NameBox.Name = "player2NameBox";
            this.player2NameBox.Size = new System.Drawing.Size(100, 20);
            this.player2NameBox.TabIndex = 3;
            this.player2NameBox.Text = "Player2";
            this.player2NameBox.TextChanged += new System.EventHandler(this.Player2NameBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Player 2 name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "White player:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Topside Player:";
            // 
            // whitePlayerCombo
            // 
            this.whitePlayerCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.whitePlayerCombo.FormattingEnabled = true;
            this.whitePlayerCombo.Items.AddRange(new object[] {
            "Player 1",
            "Player 2"});
            this.whitePlayerCombo.Location = new System.Drawing.Point(135, 90);
            this.whitePlayerCombo.Name = "whitePlayerCombo";
            this.whitePlayerCombo.Size = new System.Drawing.Size(121, 21);
            this.whitePlayerCombo.TabIndex = 8;
            this.whitePlayerCombo.SelectedIndexChanged += new System.EventHandler(this.WhitePlayerCombo_SelectedIndexChanged);
            // 
            // topsidePlayerCombo
            // 
            this.topsidePlayerCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.topsidePlayerCombo.FormattingEnabled = true;
            this.topsidePlayerCombo.Items.AddRange(new object[] {
            "Player 1",
            "Player 2"});
            this.topsidePlayerCombo.Location = new System.Drawing.Point(135, 116);
            this.topsidePlayerCombo.Name = "topsidePlayerCombo";
            this.topsidePlayerCombo.Size = new System.Drawing.Size(121, 21);
            this.topsidePlayerCombo.TabIndex = 9;
            this.topsidePlayerCombo.SelectedIndexChanged += new System.EventHandler(this.TopsidePlayerCombo_SelectedIndexChanged);
            // 
            // playedFieldColourCombo
            // 
            this.playedFieldColourCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.playedFieldColourCombo.FormattingEnabled = true;
            this.playedFieldColourCombo.Items.AddRange(new object[] {
            "White",
            "Black"});
            this.playedFieldColourCombo.Location = new System.Drawing.Point(135, 143);
            this.playedFieldColourCombo.Name = "playedFieldColourCombo";
            this.playedFieldColourCombo.Size = new System.Drawing.Size(121, 21);
            this.playedFieldColourCombo.TabIndex = 11;
            this.playedFieldColourCombo.SelectedIndexChanged += new System.EventHandler(this.PlayedFieldColourCombo_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Played Field Colour:";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(192, 325);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 47);
            this.startButton.TabIndex = 12;
            this.startButton.Text = "Start Game";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(94, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Game Settings";
            // 
            // scoreLabel1
            // 
            this.scoreLabel1.AutoSize = true;
            this.scoreLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.scoreLabel1.Location = new System.Drawing.Point(74, 218);
            this.scoreLabel1.Name = "scoreLabel1";
            this.scoreLabel1.Size = new System.Drawing.Size(35, 37);
            this.scoreLabel1.TabIndex = 14;
            this.scoreLabel1.Text = "0";
            // 
            // scoreTextLabel1
            // 
            this.scoreTextLabel1.AutoSize = true;
            this.scoreTextLabel1.Location = new System.Drawing.Point(67, 255);
            this.scoreTextLabel1.Name = "scoreTextLabel1";
            this.scoreTextLabel1.Size = new System.Drawing.Size(42, 13);
            this.scoreTextLabel1.TabIndex = 15;
            this.scoreTextLabel1.Text = "Player1";
            // 
            // scoreTextLabel2
            // 
            this.scoreTextLabel2.AutoSize = true;
            this.scoreTextLabel2.Location = new System.Drawing.Point(166, 255);
            this.scoreTextLabel2.Name = "scoreTextLabel2";
            this.scoreTextLabel2.Size = new System.Drawing.Size(42, 13);
            this.scoreTextLabel2.TabIndex = 17;
            this.scoreTextLabel2.Text = "Player2";
            // 
            // scoreLabel2
            // 
            this.scoreLabel2.AutoSize = true;
            this.scoreLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.scoreLabel2.Location = new System.Drawing.Point(169, 218);
            this.scoreLabel2.Name = "scoreLabel2";
            this.scoreLabel2.Size = new System.Drawing.Size(35, 37);
            this.scoreLabel2.TabIndex = 16;
            this.scoreLabel2.Text = "0";
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(21, 325);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 47);
            this.resetButton.TabIndex = 18;
            this.resetButton.Text = "Reset scores";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // FormPreGameSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 384);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.scoreTextLabel2);
            this.Controls.Add(this.scoreLabel2);
            this.Controls.Add(this.scoreTextLabel1);
            this.Controls.Add(this.scoreLabel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.playedFieldColourCombo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.topsidePlayerCombo);
            this.Controls.Add(this.whitePlayerCombo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.player2NameBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.player1NameBox);
            this.Controls.Add(this.label1);
            this.Name = "FormPreGameSetup";
            this.Text = "FormPreGameSetup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox player1NameBox;
        private TextBox player2NameBox;
        private Label label2;
        private Label label3;
        private Label label4;
        private ComboBox whitePlayerCombo;
        private ComboBox topsidePlayerCombo;
        private ComboBox playedFieldColourCombo;
        private Label label5;
        private Button startButton;
        private Label label6;
        private Label scoreLabel1;
        private Label scoreTextLabel1;
        private Label scoreTextLabel2;
        private Label scoreLabel2;
        private Button resetButton;
    }
}